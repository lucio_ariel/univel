#!/usr/bin/env bash
apt-get update
apt-get install -y apache2
apt-get -y install php5
apt-get -y install php5-mysql
apt-get -y install php5-curl
apt-get -y install php-apc
apt-get -y install php5-cli
apt-get -y install php-pear
apt-get -y install git

echo mysql-server mysql-server/root_password select vagrant | debconf-set-selections
echo mysql-server mysql-server/root_password_again select vagrant | debconf-set-selections
apt-get -y install mysql-server

mysql --user=root --password=vagrant -e "CREATE USER '$1'@'localhost' IDENTIFIED BY 'vagrant';"
mysql --user=root --password=vagrant -e "GRANT USAGE ON * . * TO '$1'@'localhost' IDENTIFIED BY 'vagrant' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0 ;"
mysql --user=root --password=vagrant -e "CREATE DATABASE IF NOT EXISTS $1 ;"
mysql --user=root --password=vagrant -e "GRANT ALL PRIVILEGES ON $1 . * TO '$1'@'localhost';"

a2enmod rewrite
sed -i 's/\/var\/www/\/vagrant\/www/' /etc/apache2/sites-available/default
sed -i 's/AllowOverride None/AllowOverride All/' /etc/apache2/sites-available/default
sed -i 's/APACHE_RUN_GROUP=www-data/APACHE_RUN_GROUP=vagrant/' /etc/apache2/envvars
service apache2 restart

debconf-set-selections <<EOF
phpmyadmin phpmyadmin/app-password-confirm password 
phpmyadmin phpmyadmin/dbconfig-install boolean true
phpmyadmin phpmyadmin/mysql/admin-pass password vagrant
phpmyadmin phpmyadmin/mysql/app-pass password 
phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2
EOF
apt-get -y install phpmyadmin

echo postfix postfix/main_mailer_type select "Internet Site" | debconf-set-selections
echo postfix postfix/mailname string $HOSTNAME | debconf-set-selections
apt-get -y install postfix

service apache2 restart
apt-get update --fix-missing -y

pear channel-discover pear.drush.org
pear install drush/drush
drush version
pear upgrade drush/drush

drush dl drush_language -y --destination=/home/vagrant/.drush
chown vagrant:vagrant -r /home/vagrant/.drush

apt-get update -y
apt-get && apt-get upgrade -y

apt-get install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev -y
cd
wget http://ftp.ruby-lang.org/pub/ruby/2.2/ruby-2.2.2.tar.gz
tar -xzvf ruby-2.2.2.tar.gz
cd ruby-2.2.2/
./configure
make
make install
ruby -v
cd ..
rm -r ruby-2.2.2

echo "gem: --no-ri --no-rdoc" > ~/.gemrc
gem install bundler

## instalando o sass e Compass
gem install sass
gem install compass

## instalando susy
gem install susy

## instalando o Rails se necessário
##gem install rails

apt-get -y update

## Instalando NodeJs

# Note the new setup script name for Node.js v0.12 - https://nodesource.com/blog/nodejs-v012-iojs-and-the-nodesource-linux-repositories
curl -sL --retry 10 https://deb.nodesource.com/setup_0.12 | bash -
# Then install with:
apt-get install -y nodejs

# Existe outro caminho a ser testado: https://www.vultr.com/docs/install-node-js-nvm-on-ubuntu-14-04
#curl https://raw.githubusercontent.com/creationix/nvm/v0.24.0/install.sh | bash
#source /root/.bashrc

npm update -g npm -y
## Instalando YEOMAN
npm install -g yo bower grunt-cli -y
npm install -g generator-webapp -y

## Instalando JADE
npm install -g jade